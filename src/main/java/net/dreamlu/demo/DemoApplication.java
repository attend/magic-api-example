package net.dreamlu.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.ssssssss.magicapi.spring.boot.starter.MagicAPIProperties;

/**
 * 演示服务
 *
 * @author L.cm
 */
@SpringBootApplication
public class DemoApplication implements ApplicationRunner {
    @Autowired
    private ServerProperties serverProperties;
    @Autowired
    private MagicAPIProperties magicAPIProperties;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println(String.format("服务启动完成：\thttp://localhost:%d%s", serverProperties.getPort(), magicAPIProperties.getWeb()));
    }

}
