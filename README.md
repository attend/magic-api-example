# magic-api 结合 mica 使用示例

## 快速开始

- 创建数据库，导入 `sql` 文件，详见 `sql` 目录。
- 修改 `application.yml` 配置文件。
- 启动 `DemoApplication` main 方法。
- 查看启动日志，点击打印的服务地址。

## 相关连接

* [magic-api 源码 gitee](https://gitee.com/ssssssss-team/magic-api)
* [magic-api 文档](https://ssssssss.org/guide/intro.html)
* [mica 源码 gitee](https://gitee.com/596392912/mica)
* [mica 文档](http://wiki.dreamlu.net/guide/getting-started.html)
