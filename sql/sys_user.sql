-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(32) NOT NULL COMMENT '用户账号',
  `nick_name` varchar(32) NOT NULL COMMENT '用户昵称',
  `email` varchar(64) DEFAULT '' COMMENT '用户邮箱',
  `phone` varchar(11) DEFAULT '' COMMENT '手机号码',
  `gender` tinyint(2) NOT NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(128) DEFAULT '' COMMENT '头像地址',
  `password` varchar(64) DEFAULT '' COMMENT '密码',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '用户类型（0系统用户 1管理员）',
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '帐号状态（0停用 1正常）',
  `locked` tinyint(1) NOT NULL DEFAULT '0' COMMENT '登录状态（0:正常 1:锁定）',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  `created_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES (1, 1, 'admin', '如梦技术', '596392912@qq.com', '15888888888', 1, '', '$2a$04$G9osrOEAABaO.Oi35ssV3.duz.fEF/tET7MGg7W6ulCgKrWeg.Z6e', 1, 1, 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2020-05-21 22:22:46', '超级管理员');
INSERT INTO `sys_user` VALUES (2, 1, 'mica', '如梦技术', '596392912@qq.com', '15666666666', 1, '', '$2a$04$G9osrOEAABaO.Oi35ssV3.duz.fEF/tET7MGg7W6ulCgKrWeg.Z6e', 0, 1, 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '普通用户');
INSERT INTO `sys_user` VALUES (3, 1, 'test', '如梦技术', '596392912@qq.com', '15666666666', 1, '', '$2a$04$G9osrOEAABaO.Oi35ssV3.duz.fEF/tET7MGg7W6ulCgKrWeg.Z6e', 0, 1, 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '测试用户');
COMMIT;
